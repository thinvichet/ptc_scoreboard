# config valid for current version and patch releases of Capistrano
lock "~> 3.11.2"

# config valid only for current version of Capistrano

set :application, "ptc_scoreboard"
set :repo_url, "git@bitbucket.org:thinvichet/ptc_scoreboard.git"


ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :deploy_to, "/var/www/#{fetch(:application)}"

append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle", "public/system", "public/packs", ".bundle", "node_modules", "public/uploads"
append :linked_files, '.env'

set :pty, true

set :keep_releases, 5
set :passenger_restart_with_touch, true
before "deploy:assets:precompile", "deploy:yarn_install"

namespace :deploy do
  desc "Run rake yarn install"
  task :yarn_install do
    on roles(:web) do
      within release_path do
        execute("cd #{release_path} && yarn install --silent --no-progress --no-audit --no-optional")
      end
    end
  end
end
