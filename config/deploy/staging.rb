set :stage, :staging
set :rvm_ruby_version, '2.6.5'

server '18.136.64.117', user: 'deployer', roles: %w{app web db}
