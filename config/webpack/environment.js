const { environment } = require('@rails/webpacker')
const webpack = require('webpack')
const coffee =  require('./loaders/coffee')
const datatable =  require('./loaders/datatable')

environment.plugins.append('Provide', new webpack.ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery',
  jquery: 'jquery',
  'window.jQuery': 'jquery',
  Popper: ['popper.js', 'default']
}))

/**
 * To use jQuery in views
 */
environment.loaders.append('expose', {
  test: require.resolve('jquery'),
  use: [{
    loader: 'expose-loader',
    options: 'jQuery'
  },{
    loader: 'expose-loader',
    options: '$'
  }]
})

// const config = environment.toWebpackConfig()

// config.resolve.alias = {
//   jquery: 'jquery/src/jquery',
//   flipclock: 'flipclock/dist/flipclock'
// }


environment.loaders.prepend('coffee', coffee)
environment.loaders.prepend('datatable', datatable)

module.exports = environment
