Rails.application.routes.draw do
  devise_for :admins, path: 'admins', controllers: { sessions: 'admins/sessions' }
  devise_for :users, skip: :registrations, controllers: { sessions: 'users/sessions' }

  get 'dashboards/index'
  root 'dashboards#index'

  namespace :admins do
    get 'dashboard', to: 'dashboards#index'
    resources :users
    resources :countries, controller: '/countries'
    resources :lanes
    resources :rounds
  end

  resources :countries
  resources :users, only: :show do
    resources :rounds, except: [:new, :create, :destroy]
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
