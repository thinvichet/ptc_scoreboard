CountrySelect::FORMATS[:with_data_attrs] = lambda do |country|
  country_name = ''
  case country.alpha2
  when 'GB'
    country_name = 'Great Britain'
  when 'KR'
    country_name = 'Republic Of Korea'
  when 'IR'
    country_name = 'Islamic Republic Of Iran'
  when 'TW'
    country_name = 'Chines TaiPei'
  when 'IS'
    country_name = 'England'
  when 'LA'
    country_name = 'Laos'
  else
    country_name = country.name
  end

  [
    country_name,
    country_name,
    {
      'data-value' => country.alpha2 == 'IS' ? 'gb-eng' : country.alpha2,
      'data-country-code' => country.country_code,
      'data-alpha3' => country.alpha3,
      'data-icon' => "flag-icon flag-icon-#{ country.alpha2 == 'IS' ? 'gb-eng' : country.alpha2.downcase}"
    }
  ]
end
