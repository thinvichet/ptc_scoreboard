# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Country::NAMES.each do |full_name, alpha3, alpha2|
  Country.find_or_initialize_by(full_name: full_name, alpha3: alpha3, alpha2: alpha2, flag: "flag-icon flag-icon-#{alpha2 == 'IS' ? 'gb-eng' : alpha2.downcase}") do |country|
    country.code = '1234'
    country.save!
  end
end
