class CreateAdminsLanes < ActiveRecord::Migration[6.0]
  def change
    create_table :lanes do |t|
      t.string :name, limit: 55, index: true
      t.text :description

      t.belongs_to :user, index: { unique: true }, foreign_key: true

      t.timestamps
    end
  end
end
