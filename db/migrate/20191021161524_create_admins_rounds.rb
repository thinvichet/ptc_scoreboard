class CreateAdminsRounds < ActiveRecord::Migration[6.0]
  def change
    create_table :rounds do |t|
      t.references :lane
      t.integer :player1_id, null: false, foreign_key: true
      t.integer :player2_id, null: false, foreign_key: true
      t.string :event_name, index: true
      t.datetime :date

      t.timestamps
    end

    add_index :rounds, :player1_id
    add_index :rounds, :player2_id
    add_index :rounds, [:player1_id, :player2_id, :lane_id], name: "index_player1_player2_lane", unique: true, using: :btree
  end
end
