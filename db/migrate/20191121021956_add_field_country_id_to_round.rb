class AddFieldCountryIdToRound < ActiveRecord::Migration[6.0]
  def change
    add_column :rounds, :country_id, :integer
    add_index :rounds, :country_id
  end
end
