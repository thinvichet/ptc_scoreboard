class CreateCountries < ActiveRecord::Migration[6.0]
  def change
    create_table :countries do |t|
      t.string :full_name, index: true
      t.string :code, limit: 4
      t.string :alpha2, limit: 2, index: true
      t.string :alpha3, limit: 3
      t.string :flag, default: ''

      t.timestamps
    end
  end
end
