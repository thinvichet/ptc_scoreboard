class CreatePlayerRounds < ActiveRecord::Migration[6.0]
  def change
    create_table :player_rounds do |t|
      t.belongs_to :round, null: false, foreign_key: true
      t.integer :set, default: 0
      t.integer :player1_score, default: 0
      t.integer :player2_score, default: 0
      t.integer :winner_id, null: true
      t.time :total_timme

      t.timestamps
    end
    add_index :player_rounds, :winner_id
  end
end
