class AddNamePhoneNumberToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :name, :string, limit: 52, default: ''
    add_column :users, :phone_number, :string, limit: 12
  end
end
