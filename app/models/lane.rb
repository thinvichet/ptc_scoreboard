class Lane < ApplicationRecord

  belongs_to :user
  has_many :rounds, dependent: :restrict_with_error

  validates :name, :user_id, presence: true
end
