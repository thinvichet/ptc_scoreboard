class PlayerRound < ApplicationRecord
  belongs_to :round
  belongs_to :winner, class_name: 'Country', foreign_key: 'winner_id',  optional: true

  validates :player1_score, :player2_score, numericality: { greater_than_or_equal_to: 0 }
  default_scope { order(:set) }
end
