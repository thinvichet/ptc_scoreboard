class Round < ApplicationRecord

  belongs_to :lane
  has_many :player_rounds, dependent: :destroy

  belongs_to :player1,    class_name: 'Country', foreign_key: 'player1_id'
  belongs_to :player2,    class_name: 'Country', foreign_key: 'player2_id'

  accepts_nested_attributes_for :player_rounds

  EVENT_NAMES = ["Men's Shooting", "Women's Shooting", "Junior Shooting", "Men's Triple", "Women's Triple", "Junior Triple", "Men's Single", "Women's Single", "Junior Single", "Men's Double", "Women's Double", "Mix Triple", "Mix Double"]
  validates :lane_id, :event_name, presence: true
end
