import 'bootstrap-select/dist/js/bootstrap-select.min'
import 'bootstrap-select/sass/bootstrap-select'
import 'jquery.fullscreen/release/jquery.fullscreen.min'
import './bootstrap-notify'

$.fn.selectpicker.Constructor.BootstrapVersion = '4';

$(document).ready(function () {
  $('.selectpicker').selectpicker();
  var formActions = ['rounds-new', 'rounds-create', 'rounds-edit', 'rounds-update']
  $('.selectpicker').selectpicker('refresh')
  if (formActions.includes($('body').attr('id'))){

    $("button.btn.btn-icon.btn-simple.btn-github").on('click', function(e){
      e.preventDefault();
      var data = $(this).data();
      var value = $("input#" + data.inputId).val()
      if (value >= 0) {
        value = eval(value + " " + data.operator + " " + "1" )
        value = value == -1 ? 0 : value
        $("#" + data.inputId + " span").text(value)
        $("input#" + data.inputId).attr('value', value);
      }
    });

  }

  if($('.wrapper-player-round.d-none').length == 0 ){
    $('.wrapper-player-round').addClass('d-none')
    $('.wrapper-player-round:first').removeClass('d-none')
  }

  $('#btn-play-timer').on('click', function(e){
    e.preventDefault();
    $.ajax({
      url: `${$('form#round-form').attr('action')}?&timer=start`,
      type: 'post',
      dataType: 'json',
      data: $('form#round-form').serialize(),
      success: function(data) {
                // ... do something with the data...
               }
    });
  });

  $('input[type="radio"]').on('click', function(){
    if(confirm(`Are you sure ${$(this).data('country-name')} is the winner?`)){
      postRoundValues(true, $(this).data('index'))
    }
    else{
      return false;
    }
  });

  $('.player1-score-wrapper button, .player2-score-wrapper button').on('click', function() {
    postRoundValues(false, $(this).data('index'))
  });

  function postRoundValues(winner=false, index=null) {
    $.ajax({
      url: `${$('form#round-form').attr('action')}?&winner=${winner}&index=${index}`,
      type: 'post',
      dataType: 'json',
      data: $('form#round-form').serialize(),
      success: function(data) {
                // ... do something with the data...
               }
    });
  }

  // table.buttons().container()./appendTo('#datatable_wrapper .col-md-6:eq(0)');
  $(function() {
    // check native support
    $('#support').text($.fullscreen.isNativelySupported() ? 'supports' : 'doesn\'t support');
    // open in fullscreen
    $('#fullscreen .requestfullscreen').click(function() {
      $('#fullscreen').fullscreen();
      return false;
    });
    // exit fullscreen
    $('#fullscreen .exitfullscreen').click(function() {
      $.fullscreen.exit();
      return false;
    });
    // document's event
    $(document).bind('fscreenchange', function(e, state, elem) {
      // if we currently in fullscreen mode
      if ($.fullscreen.isFullScreen()) {
        $('#fullscreen .requestfullscreen').hide();
        $('#fullscreen .exitfullscreen').show();
      } else {
        $('#fullscreen .requestfullscreen').show();
        $('#fullscreen .exitfullscreen').hide();
      }
      $('#state').text($.fullscreen.isFullScreen() ? '' : 'not');
    });
  });

})

