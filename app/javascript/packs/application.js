// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("@rails/actioncable")
require("channels")
require.context('../images', true)

// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
import 'bootstrap'
// javascripts libraries
// theme js
import './bootstrap-notify'
import '../stylesheets/application'

import './datatable'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import 'flag-icon-css/sass/flag-icon'

// import 'select2'
// import 'select2/src/scss/core'
// import '@ttskch/select2-bootstrap4-theme/src/layout'
import './black-dashboard.min'


document.addEventListener("turbolinks:load", () => {
  if ($('.wrapper').data('notice')) {
    showNotification($('.wrapper').data('notice'), 'success')
  } else if ($('.wrapper').data('alert')){
    showNotification($('.wrapper').data('alert'), 'warning')
  }

  function showNotification(message, color){
    $.notify({
      icon: "tim-icons icon-bell-55",
      message: message
    },{
      type: color,
      timer: 500,
      placement: {
        from: 'top',
        align: 'right'
      }
    });
  }

  if ($('body').attr('id') && $('body').attr('id').match(/\-index/)) {
    var table = $('.datatable').DataTable({
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      },
      dom: "<'row'<'col-sm-12 col-md-7'lB><'col-sm-12 col-md-5'<'row'<'col-12'f>>>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      // buttons: [
      //   'csv', 'excel'
      // ]
      buttons: [
              {
                  extend: 'csv',
                  className: 'btn-sm btn-primary',
                  text: 'Export CSV',
                  title: 'User list',
                  init: function (api, node, config) {
                    $(node).removeClass('btn-secondary')
                  }
              }
          ],
    });
  }

  $('#footer-year').text(new Date().getFullYear());


  // $('select').select2({
  //     theme: 'bootstrap4',
  // });
})

