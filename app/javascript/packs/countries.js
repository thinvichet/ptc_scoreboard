import 'bootstrap-select/dist/js/bootstrap-select.min'
import 'bootstrap-select/sass/bootstrap-select'
import './dashboards/jquery-jvectormap'
import './dashboards/demo'
import './jquery.alterclass'

$.fn.selectpicker.Constructor.BootstrapVersion = '4';

$(document).ready(function () {
  demo.initVectorMap($('#worldMap').data('map'));

  $('#country_full_name').selectpicker().on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    var selectedOption = undefined

    selectedOption = $(e.target.selectedOptions[0])
    $('#country_code').val(selectedOption.data('country-code'));
    $('#country_alpha2').val(selectedOption.data('value'));
    $('#country_alpha3').val(selectedOption.data('alpha3'));
    $('#country_flag').val(selectedOption.data('icon'));
    $('#flag-placeholder').alterClass('flag-icon-*', 'flag-icon flag-icon-' + selectedOption.data('value').toLowerCase()).css('font-size', '10em')
    $('#image-placeholder').remove()
  });

})
