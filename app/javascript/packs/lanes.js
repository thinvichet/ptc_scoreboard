import 'bootstrap-select/dist/js/bootstrap-select.min'
import 'bootstrap-select/sass/bootstrap-select'

$(document).ready(function () {
  var formActions = ['lanes-new', 'lanes-create', 'lanes-edit', 'lanes-update']
  if (formActions.includes($('body').attr('id'))){s
    $('.selectpicker').selectpicker('refresh')
  }
});
