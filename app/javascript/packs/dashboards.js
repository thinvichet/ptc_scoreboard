import './dashboards/moment.min'
import './dashboards/chartjs.min'
import './dashboards/jquery-jvectormap'
import './dashboards/demo'

$(document).ready(function () {
  demo.initDashboardPageCharts();
  demo.initVectorMap();
})
