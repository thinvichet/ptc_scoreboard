import consumer from "./consumer"
import 'flipclock/dist/flipclock'

let round_id;

$(document).on('turbolinks:load', function () {
  // TODO look for round id from page and subscribe to the channel of that round

  console.log("channel ID " + round_id())
  console.log("Round channel  " + $('#round-header').data('round-id'))
  var clock = $(`#count-up-clock-${$('#round-header').attr('data-round-id')}`).FlipClock({
    clockFace: 'MinuteCounter',
    autoStart: 'false'
  });
  clock.stop()

  consumer.subscriptions.create({
    channel: "RoundChannel",
    round: $('#round-header').attr('data-round-id')
  }, {
      connected() {
        // Called when the subscription is ready for use on the server
        console.log("Connected to the round " + $('#round-header').data('round-id'))
        // $(`#round-id-${$('#round-header').attr('data-round-id')} .wrapper-player-round#display-set-0`).removeClass('d-none')
        // clock.start()
      },
      disconnected() {
        // Called when the subscription has been terminated by the server
        clock.stop()
      },
      received(data) {
        // Called when there's incoming data on the websocket for this channel
        if (data.body[0]['winner_id'] == null) {
          $(`#display-set-1`).addClass('d-none')
          $(`#display-set-2`).addClass('d-none')
          $(`#display-set-0`).removeClass('d-none')

        }else if (data.body[0]['winner_id'] != null && data.body[1]['winner_id'] == null){
          $(`#display-set-0`).addClass('d-none')
          $(`#display-set-2`).addClass('d-none')
          $(`#display-set-1`).removeClass('d-none')
          if (data.winner_is == 'true') {
            if(data.body[0]['winner_id'] === data.player1_id) {
              $('#winner1-indicator').append('<i class="fas fa-circle text-info"></i>')
            }else if(data.body[0]['winner_id'] === data.player2_id){
              $('#winner2-indicator').append('<i class="fas fa-circle text-info"></i>')
            }
            clock.reset();
          }
        }else if (data.body[1]['winner_id'] != null && data.body[2]['winner_id'] == null){
          if (data.winner_is == 'true') {
            if(data.body[1]['winner_id'] === data.player1_id){
              $('#winner1-indicator').append('<i class="fas fa-circle text-info"></i>')
            }else if(data.body[1]['winner_id'] === data.player2_id){
              $('#winner2-indicator').append('<i class="fas fa-circle text-info"></i>')
            }
            clock.reset();
          }
          if ($('#winner1-indicator').children().length == 2 || $('#winner2-indicator').children().length == 2) {
            $(`#display-set-0`).addClass('d-none')
            $(`#display-set-1`).removeClass('d-none')
            $(`#display-set-2`).addClass('d-none')
          }else {
            $(`#display-set-0`).addClass('d-none')
            $(`#display-set-1`).addClass('d-none')
            $(`#display-set-2`).removeClass('d-none')
          }

        }else if ($('#winner1-indicator').children().length != 2){
          if (data.winner_is == 'true') {
            if(data.body[2]['winner_id'] === data.player1_id){
              $('#winner1-indicator').append('<i class="fas fa-circle text-info"></i>')
            }else if(data.body[2]['winner_id'] === data.player2_id){
              $('#winner2-indicator').append('<i class="fas fa-circle text-info"></i>')
            }
            clock.reset();
          }
        }else if($('#winner2-indicator').children().length != 2) {
          if (data.winner_is == 'true') {
            if(data.body[2]['winner_id'] === data.player1_id){
              $('#winner1-indicator').append('<i class="fas fa-circle text-info"></i>')
            }else if(data.body[2]['winner_id'] === data.player2_id){
              $('#winner2-indicator').append('<i class="fas fa-circle text-info"></i>')
            }
            clock.reset();
          }
        }

        if(data.winner_is == 'true'){
          showNotification(`Congratulation to ${data.body[data.winner_index]['winner']}!!!`, 'success')
          if (data.winner_index >= 1) { clock.stop() }
        }

        if (data.timer == 'start') {
          debugger;
          clock.start();
        }

        if(data.index != 'false'){
          $(`#round-${data.body[data.index]['round_id']}-player-1-set-${data.body[data.index]['set']}`).text(data.body[data.index]['player1_score']);
          $(`#round-${data.body[data.index]['round_id']}-player-2-set-${data.body[data.index]['set']}`).text(data.body[data.index]['player2_score']);
        }

        // $(`#round-${data.body[1]['round_id']}-player-1-set-${data.body[1]['set']}`).text(data.body[1]['player1_score']);
        // $(`#round-${data.body[1]['round_id']}-player-2-set-${data.body[1]['set']}`).text(data.body[1]['player2_score']);
      }
    });

  function showNotification(message, color){
    $.notify({
      icon: "tim-icons icon-bell-55",
      title: 'Here is the winner:',
      message: message
    },{
      offset: 300,
      element: '.card-body',
      type: color,
      timer: 550,
      animate: {
        enter: 'animated fadeInDown',
        exit: 'animated zoomOutRight'
      },
      placement: {
        from: 'top',
        align: 'center'
      },
      template: '<div data-notify="container" class="col-xs-11 col-sm-3 col-md-8 alert alert-{0}" role="alert">' +
          '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
          '<span data-notify="icon"></span> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message"><h1>{2}</h1></span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }
})

round_id = function () {
  $('#round-header').data('round-id')
}


