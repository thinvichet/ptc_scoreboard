class RoundChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "rounds:#{params['round_id'].to_i}:player_rounds"
    stream_for round
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end

  private

  def round
    puts "\n" *8
    p params[:round]
    Round.find_by(id: params[:round])
  end
end
