class Admins::UsersController < Admins::AdminsController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /admins/users
  # GET /admins/users.json
  def index
    @users = User.all
  end

  # GET /admins/users/1
  # GET /admins/users/1.json
  def show
  end

  # GET /admins/users/new
  def new
    @user = User.new
  end

  # GET /admins/users/1/edit
  def edit
  end

  # POST /admins/users
  # POST /admins/users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admins_user_path(@user), notice: 'User was successfully created.' }
        format.json { render :show, status: :created }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admins/users/1
  # PATCH/PUT /admins/users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to admins_user_path(@user), notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admins/users/1
  # DELETE /admins/users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admins_users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :phone_number, :password, :password_confirmation)
    end
end
