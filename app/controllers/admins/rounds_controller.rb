class Admins::RoundsController < Admins::AdminsController
  before_action :set_round, only: [:show, :edit, :update, :destroy]

  # GET /admins/rounds
  # GET /admins/rounds.json
  def index
    @rounds = Round.all
  end

  # GET /admins/rounds/1
  # GET /admins/rounds/1.json
  def show
  end

  # GET /admins/rounds/new
  def new
    @round = Round.new
    [1, 2, 3].each do |set|
      @round.player_rounds.build(set: set)
    end
  end

  # GET /admins/rounds/1/edit
  def edit
  end

  # POST /admins/rounds
  # POST /admins/rounds.json
  def create
    @round = Round.new(round_params)
    respond_to do |format|
      if @round.save
        format.html { redirect_to admins_round_path(@round), notice: 'Round was successfully created.' }
        format.json { render :show, status: :created }
      else
        format.html { render :new }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admins/rounds/1
  # PATCH/PUT /admins/rounds/1.json
  def update
    respond_to do |format|
      if @round.update(round_params)
        format.html { redirect_to edit_admins_round_path(@round), notice: 'Round was successfully updated.' }
        format.json { render :show, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admins/rounds/1
  # DELETE /admins/rounds/1.json
  def destroy
    @round.destroy
    respond_to do |format|
      format.html { redirect_to admins_rounds_url, notice: 'Round was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round
      @round = Round.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_params
      params.require(:round).permit(
        :lane_id, :event_name, :date, :player1_id,
        :player2_id, :winner_id, player_rounds_attributes: [:id, :set, :player1_score, :player2_score, :winner_id]
      )
    end
end
