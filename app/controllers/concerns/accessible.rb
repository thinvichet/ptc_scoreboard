module Accessible
  extend ActiveSupport::Concern
  included do
    before_action :check_user
  end

  protected
  def check_user
    if !user_signed_in?
      if current_admin
        # flash.clear
        flash[:success] = I18n.t('devise.sessions.admin.signed_in')
        # if you have rails_admin. You can redirect anywhere really
        redirect_to(admins_dashboard_path) && return
      elsif current_user
        # flash.clear
        flash[:success] = I18n.t('devise.sessions.signed_in')
        # The authenticated root path can be defined in your routes.rb in: devise_scope :user do...
        redirect_to(user_rounds(current_user)) && return
      end
    end
  end
end
