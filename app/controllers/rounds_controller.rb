class RoundsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_round, only: [:show, :edit, :update, :destroy]

  # GET /rounds
  # GET /rounds.json
  def index
    @rounds = Round.all
  end

  # GET /rounds/1
  # GET /rounds/1.json
  def show
  end

  # GET /rounds/new
  # def new
  #   @round = Round.new
  #   [1, 2].each do |set|
  #     @round.player_rounds.build(set: set)
  #   end
  # end

  # GET /rounds/1/edit
  def edit
  end

  # PATCH/PUT /rounds/1
  # PATCH/PUT /rounds/1.json
  def update
    respond_to do |format|
      if @round.update(round_params)
        player_rounds = @round.player_rounds.map do |pr|
          {
            id: pr.id, round_id: pr.round_id,
            player1_score: pr.player1_score,
            player2_score: pr.player2_score,
            winner_id: pr.winner_id, set: pr.set,
            winner: pr.winner.try(:full_name),
            total_time: ''
          }
        end

        RoundChannel.broadcast_to @round, body: player_rounds, winner_index: params['index'] != 'null' ? params['index'].to_i : '',
                                          winner_is: params['winner'], timer: params['timer'], user: @round.lane.user.email,
                                          index: params['index'].to_i, player1_id: @round.player1_id, player2_id: @round.player2_id

        format.html { redirect_to edit_user_round_path(current_user, @round), notice: 'Round was successfully updated.' }
        format.json do
          render json: @round, status: :ok
        end
      else
        format.html { render :edit }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rounds/1
  # DELETE /rounds/1.json
  # def destroy
  #   @round.destroy
  #   respond_to do |format|
  #     format.html { redirect_to admins_rounds_url, notice: 'Round was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_round
      @round = Round.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def round_params
      params.require(:round).permit(
        :lane_id, :event_name, :date, :player1_id,
        :player2_id, :winner_id, player_rounds_attributes: [:id, :set, :player1_score, :player2_score, :winner_id]
      )
    end
end
