class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :layout

  protected

    def authenticate_inviter!
      authenticate_admin!(force: true)
    end

  private

    def layout
      controller_names = %w(sessions passwords confirmations unlocks)
      controller_action_name = "#{controller_name}-#{action_name}"
      if controller_action_name == 'invitations-edit' || controller_action_name == 'invitations-update'
        'devise_confirm'
      elsif controller_names.include?(controller_name)
        'login'
      else
        "application"
      end
    end

    def after_sign_in_path_for(resource_or_scope)
      stored_location_for(resource_or_scope) || super
    end

    def after_sign_out_path_for(resource_or_scope)
      request.referrer
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(
        :invite, keys: [
          :skip_en_name_validation,
          :name, :role
          ]
        )
      devise_parameter_sanitizer.permit(
        :accept_invitation, keys: [
          :skip_en_name_validation
        ]
      )
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :role, :organization_id])
    end
end
