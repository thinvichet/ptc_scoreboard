class RoundRelayJob < ApplicationJob
  queue_as :default

  def perform(player_round)
    # Do something later
    ActionCable.server.broadcast "rounds:#{player_round.round_id}:player_rounds"
  end
end
