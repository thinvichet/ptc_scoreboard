require "rails_helper"

RSpec.describe Admins::LanesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/admins/lanes").to route_to("admins/lanes#index")
    end

    it "routes to #new" do
      expect(:get => "/admins/lanes/new").to route_to("admins/lanes#new")
    end

    it "routes to #show" do
      expect(:get => "/admins/lanes/1").to route_to("admins/lanes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admins/lanes/1/edit").to route_to("admins/lanes#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/admins/lanes").to route_to("admins/lanes#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admins/lanes/1").to route_to("admins/lanes#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admins/lanes/1").to route_to("admins/lanes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admins/lanes/1").to route_to("admins/lanes#destroy", :id => "1")
    end
  end
end
