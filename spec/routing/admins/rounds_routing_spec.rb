require "rails_helper"

RSpec.describe Admins::RoundsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/admins/rounds").to route_to("admins/rounds#index")
    end

    it "routes to #new" do
      expect(:get => "/admins/rounds/new").to route_to("admins/rounds#new")
    end

    it "routes to #show" do
      expect(:get => "/admins/rounds/1").to route_to("admins/rounds#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admins/rounds/1/edit").to route_to("admins/rounds#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/admins/rounds").to route_to("admins/rounds#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admins/rounds/1").to route_to("admins/rounds#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admins/rounds/1").to route_to("admins/rounds#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admins/rounds/1").to route_to("admins/rounds#destroy", :id => "1")
    end
  end
end
