require 'rails_helper'

RSpec.describe "admins/lanes/new", type: :view do
  before(:each) do
    assign(:admins_lane, Admins::Lane.new(
      :name => "MyString",
      :description => "MyString",
      :user => nil
    ))
  end

  it "renders new admins_lane form" do
    render

    assert_select "form[action=?][method=?]", admins_lanes_path, "post" do

      assert_select "input[name=?]", "admins_lane[name]"

      assert_select "input[name=?]", "admins_lane[description]"

      assert_select "input[name=?]", "admins_lane[user_id]"
    end
  end
end
