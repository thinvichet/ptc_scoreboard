require 'rails_helper'

RSpec.describe "admins/lanes/edit", type: :view do
  before(:each) do
    @admins_lane = assign(:admins_lane, Admins::Lane.create!(
      :name => "MyString",
      :description => "MyString",
      :user => nil
    ))
  end

  it "renders the edit admins_lane form" do
    render

    assert_select "form[action=?][method=?]", admins_lane_path(@admins_lane), "post" do

      assert_select "input[name=?]", "admins_lane[name]"

      assert_select "input[name=?]", "admins_lane[description]"

      assert_select "input[name=?]", "admins_lane[user_id]"
    end
  end
end
