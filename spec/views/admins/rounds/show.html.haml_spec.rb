require 'rails_helper'

RSpec.describe "admins/rounds/show", type: :view do
  before(:each) do
    @admins_round = assign(:admins_round, Admins::Round.create!(
      :lane => "",
      :event_name => "Event Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Event Name/)
  end
end
