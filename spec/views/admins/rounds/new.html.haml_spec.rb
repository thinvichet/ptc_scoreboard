require 'rails_helper'

RSpec.describe "admins/rounds/new", type: :view do
  before(:each) do
    assign(:admins_round, Admins::Round.new(
      :lane => "",
      :event_name => "MyString"
    ))
  end

  it "renders new admins_round form" do
    render

    assert_select "form[action=?][method=?]", admins_rounds_path, "post" do

      assert_select "input[name=?]", "admins_round[lane]"

      assert_select "input[name=?]", "admins_round[event_name]"
    end
  end
end
