require 'rails_helper'

RSpec.describe "admins/rounds/edit", type: :view do
  before(:each) do
    @admins_round = assign(:admins_round, Admins::Round.create!(
      :lane => "",
      :event_name => "MyString"
    ))
  end

  it "renders the edit admins_round form" do
    render

    assert_select "form[action=?][method=?]", admins_round_path(@admins_round), "post" do

      assert_select "input[name=?]", "admins_round[lane]"

      assert_select "input[name=?]", "admins_round[event_name]"
    end
  end
end
