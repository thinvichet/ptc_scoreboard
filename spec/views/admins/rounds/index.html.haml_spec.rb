require 'rails_helper'

RSpec.describe "admins/rounds/index", type: :view do
  before(:each) do
    assign(:admins_rounds, [
      Admins::Round.create!(
        :lane => "",
        :event_name => "Event Name"
      ),
      Admins::Round.create!(
        :lane => "",
        :event_name => "Event Name"
      )
    ])
  end

  it "renders a list of admins/rounds" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "Event Name".to_s, :count => 2
  end
end
