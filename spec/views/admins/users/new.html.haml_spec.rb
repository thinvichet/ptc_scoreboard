require 'rails_helper'

RSpec.describe "admins/users/new", type: :view do
  before(:each) do
    assign(:admins_user, Admins::User.new(
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString"
    ))
  end

  it "renders new admins_user form" do
    render

    assert_select "form[action=?][method=?]", admins_users_path, "post" do

      assert_select "input[name=?]", "admins_user[name]"

      assert_select "input[name=?]", "admins_user[email]"

      assert_select "input[name=?]", "admins_user[phone]"
    end
  end
end
