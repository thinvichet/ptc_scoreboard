require 'rails_helper'

RSpec.describe "admins/users/edit", type: :view do
  before(:each) do
    @admins_user = assign(:admins_user, Admins::User.create!(
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString"
    ))
  end

  it "renders the edit admins_user form" do
    render

    assert_select "form[action=?][method=?]", admins_user_path(@admins_user), "post" do

      assert_select "input[name=?]", "admins_user[name]"

      assert_select "input[name=?]", "admins_user[email]"

      assert_select "input[name=?]", "admins_user[phone]"
    end
  end
end
