require 'rails_helper'

RSpec.describe "admins/users/show", type: :view do
  before(:each) do
    @admins_user = assign(:admins_user, Admins::User.create!(
      :name => "Name",
      :email => "Email",
      :phone => "Phone"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Phone/)
  end
end
