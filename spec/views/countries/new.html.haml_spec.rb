require 'rails_helper'

RSpec.describe "countries/new", type: :view do
  before(:each) do
    assign(:country, Country.new(
      :full_name => "MyString",
      :short_name => "MyString",
      :flag => "MyString"
    ))
  end

  it "renders new country form" do
    render

    assert_select "form[action=?][method=?]", countries_path, "post" do

      assert_select "input[name=?]", "country[full_name]"

      assert_select "input[name=?]", "country[short_name]"

      assert_select "input[name=?]", "country[flag]"
    end
  end
end
