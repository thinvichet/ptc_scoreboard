require 'rails_helper'

RSpec.describe "countries/show", type: :view do
  before(:each) do
    @country = assign(:country, Country.create!(
      :full_name => "Full Name",
      :short_name => "Short Name",
      :flag => "Flag"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Full Name/)
    expect(rendered).to match(/Short Name/)
    expect(rendered).to match(/Flag/)
  end
end
