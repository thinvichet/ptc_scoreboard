require 'rails_helper'

RSpec.describe "countries/edit", type: :view do
  before(:each) do
    @country = assign(:country, Country.create!(
      :full_name => "MyString",
      :short_name => "MyString",
      :flag => "MyString"
    ))
  end

  it "renders the edit country form" do
    render

    assert_select "form[action=?][method=?]", country_path(@country), "post" do

      assert_select "input[name=?]", "country[full_name]"

      assert_select "input[name=?]", "country[short_name]"

      assert_select "input[name=?]", "country[flag]"
    end
  end
end
