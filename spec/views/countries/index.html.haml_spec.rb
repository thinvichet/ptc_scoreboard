require 'rails_helper'

RSpec.describe "countries/index", type: :view do
  before(:each) do
    assign(:countries, [
      Country.create!(
        :full_name => "Full Name",
        :short_name => "Short Name",
        :flag => "Flag"
      ),
      Country.create!(
        :full_name => "Full Name",
        :short_name => "Short Name",
        :flag => "Flag"
      )
    ])
  end

  it "renders a list of countries" do
    render
    assert_select "tr>td", :text => "Full Name".to_s, :count => 2
    assert_select "tr>td", :text => "Short Name".to_s, :count => 2
    assert_select "tr>td", :text => "Flag".to_s, :count => 2
  end
end
